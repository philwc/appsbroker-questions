# Question 1 - Hello World!

The terraform included in this directory will provision a new public http cloud function containing a simple hello world application.

I chose this deployment method for ease of deployment and management, low cost and automatic scaling capabilities.

This wouldn't be appropriate for more complex applications, or applications requiring a longer runtime or increased memory. For these requirements, I would start to look at Cloud Run or GKE, based on any other factors.
