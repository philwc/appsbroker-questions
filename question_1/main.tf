resource "google_storage_bucket" "code_bucket" {
  project  = var.project_id
  name     = "${var.project_id}-code-bucket"
  location = "EU"
}

data "archive_file" "hello_world" {
  type        = "zip"
  output_path = "${path.module}/functions/hello_world.zip"
  dynamic "source" {
    for_each = fileset(path.module, "functions/hello_world/**/*.{txt,py}")
    content {
      content  = file("${path.module}/${source.value}")
      filename = basename(source.value)
    }
  }
}

resource "google_storage_bucket_object" "hello_world" {
  name   = "hello_world.zip#${data.archive_file.hello_world.output_md5}"
  bucket = google_storage_bucket.code_bucket.name
  source = data.archive_file.hello_world.output_path
}

resource "google_cloudfunctions_function" "hello_world" {
  source_archive_bucket = google_storage_bucket.code_bucket.name
  source_archive_object = google_storage_bucket_object.hello_world.name
  entry_point           = "hello_world"
  name                  = "hello_world"
  project               = var.project_id
  region                = "europe-west2"
  runtime               = "python39"
  trigger_http          = true
}
# IAM entry for all users to invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = var.project_id
  region         = google_cloudfunctions_function.hello_world.region
  cloud_function = google_cloudfunctions_function.hello_world.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}
