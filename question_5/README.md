# Question 5 - Security

In the current design, there is no external access defined. This already goes a long way to prevent data exfiltration. To further bolster this, Cloud Data Loss Prevention tools (<https://cloud.google.com/dlp>) may be used to scan for, and potentially mask PII.

In addition to this, good security hygiene must be maintained, including: - Not using default service accounts - instead create accounts for specific use cases with restricted permissions - Restrict access to the platform for end users - Ensure audit logging is working and setup as intended - Make use of tools such as Security Command Center

If this application is required to be opened to the world eventually, consideration must be given to authentication and authorization (Cloud Identity Platform/IAM may be used to help with this). In addition the GCP Web Security Scanner may be used to perform regular scans of the exposed application. Finally tools such as Cloud Armor may be used to block access to known bad actors as well as other suspicious activity.
