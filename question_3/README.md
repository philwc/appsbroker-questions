# Question 3 - Scaling

Cloud run supports autoscaling natively without any additional work required.

From a control point of view, we can allocate how many requests a container may receive (concurrency), as well how many containers we may scale up and down to.

By default, cloud run will scale at 60% CPU utilization

More information is available at <https://cloud.google.com/run/docs/about-instance-autoscaling>
