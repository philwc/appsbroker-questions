# Appsbroker Technical Questions

1. You work for a company looking into the benefits of public cloud. After much thought,they have settled on exploring Google Cloud Platform (GCP) as a provider. They have asked you to assist in developing a technical proof of concept solution from
   scratch.

   **Please show a public “Hello world” application running on GCP.**

   [Answer](question_1)

2. A discovery workshop as part of this exploration has resulted in a business requirement that identified the application needs to run on a private RFC1918 based network range with a GCP Database Service as well as access to a blob store for static content.

   **Please show how you incorporate this into your design and present this along
   with any associated code.**

   [Answer](question_2)

3. The company is impressed with your efforts so far. To meet expectations, they have asked if this solution could provide a greater deal of business continuity and will scale under one of the following conditions; Utilisation, Traffic and/or Status.

   **Please show how your application/solution meets this expectation of business
   continuity.**

   [Answer](question_3)

4. The Financial Director has been made aware of the benefits of public cloud and the
   potential use cases of your application. They are a keen advocate of ‘cost optimisation’ and would like to know if your design has factored this in.

   **Please review your previous designs to identify areas of optimisation for cost and how you could look to reduce it. Present this back.**

   [Answer](question_4)

5. Your ‘simple POC’ has been generating a lot of positive noise internally! The risk and compliance team have been alerted to your efforts and are interested in the practical uses of your application, provided it meets their compliance expectations. After review, it has been identified this application will be storing Personal Identifiable Information (PII). In a world of data breaches, the team want to know how you meet this concern.

   **Please review your design and highlight/present a solution to a “Technical Design Authority” audience that would resolve any Security, Data Exfiltration and Compliance concerns.**

   [Answer](question_5)
