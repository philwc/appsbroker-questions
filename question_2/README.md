# Question 2 - Private Architecture

Cloud run can run in an internal traffic only mode, restricting access to the VPC. In order to grant access to external services, a VPC connector must be setup and configured.

See the below diagram:

![Private Cloud Run Arch](PrivateCloudRunArch.png)
