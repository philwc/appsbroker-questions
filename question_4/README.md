# Question 4 - Cost Optimisation

Using cloud run is a good start in terms of cost optimisation, given its scale to zero capabilities, we will not be charged whilst there are no requests.

The other services proposed (Cloud Storage & Cloud Datastore) are priced based on stored data and accesses. This may be optimised based on data redundancy/availability requirements. For example, we may want to set the storage bucket to a single region, and set the storage class to nearline or coldline, based on access requirements.
